# GPSA Knowledge Platform Data Hub.
(C) 2018 - Miguel Chavez Gamboa
miguel@codea.me

This app is the frontend only, written in Angular 5. It makes use of the Data Hub API which is served at api.gpsaknowledge.org

## License

This app is licensed under the GNU AGPL version 3 or later.

Copyright (C) 2018 Miguel Chavez Gamboa
miguel@codea.me

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>. 


### This app is based on SB Admin Dashboard App

This project is a port of the famous Free Admin Bootstrap Theme [SB Admin v5.0](http://startbootstrap.com/template-overviews/sb-admin-2/) to Angular5 Theme.

Powered by [StartAngular](http://startangular.com/) & [StrapUI](http://strapui.com/)


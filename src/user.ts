export interface IStatistics {
  user_count: number;
  users_with_attendees: number;
  users_with_comments: number;
  users_with_logins: number;
  unique_attendees: Object;
  webinar_users: Object;
  active_users: Object;
  inactive_users: Object;
  super_users: any;
  lurker_users: any;
  arrepentidos: any;
  onetime_webinar_users: any;
  webinar_superuser: any;
  users_only_one_webinar: any;
  users_etiquetas: Array<String>;
  chart_data: Object;
  alterno:Array<Object>;
}

export interface IWebinar {
  webinar_attendees:Array<Object>;
  empty_rooms:Array<object>;
  // room_name: string;
  // starts_at:Date;
  // count:number;
}

export interface IClickMeetingUsers {
  users:Array<Object>;
  totals:number;
}

export interface IKpUsers {
  users:Array<Object>;
  totals:number;
}

export interface ICampaigns {
  campaigns:Object;
  cid: string;
  web_id:string;
  create_time:Date;
  send_time:Date;
  emails_sent:number;
  report_summary: Object;
  title:string;
  from_name:string;
  status:string;
  account_name:string;
}

export interface IUsers {
  email: string;
  first_name: string;
  last_name: string;
  country: string;
  nickname: string;
  user_id: string;
  username: string;
  display_name: string;
  description: string;
  about: string;
  organization: string;
  org_type: string;
  org_position: string;
  role: string;
  registered_at: Object; //string; // DATE!!!
  attendee_room_list: Array<String>;
}

export interface IAttendees {
  uid: string;
  user_ip: string;
  nickname: string;
  email: string;
  start_date: Date;
  end_date: Date;
}

export interface ITest {
  email: string;
  country: string;
}

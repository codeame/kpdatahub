import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

import { ScopeGuardService as ScopeGuard } from '../shared';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'dashboard' },
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            // { path: 'mailchimp/overview', loadChildren: './mailchimp/mailchimp.module#MailChimpModule' },
            { path: 'mailchimp/campaigns', loadChildren: './mailchimp_campaigns/mailchimp_campaigns.module#MailChimpModule', canActivate: [ScopeGuard], data: { expectedScopes: ['read:all_data']} },
            { path: 'clickmeeting/webinars', loadChildren: './clickmeeting/clickmeeting.module#ClickMeetingModule', canActivate: [ScopeGuard], data: { expectedScopes: ['read:all_data']} },
            { path: 'clickmeeting/attendees', loadChildren: './clickmeeting_users/clickmeeting_users.module#ClickMeetingUsersModule', canActivate: [ScopeGuard], data: { expectedScopes: ['read:all_data']} },
            { path: 'kp/users', loadChildren: './kp_users/kp_users.module#KpUsersModule', canActivate: [ScopeGuard], data: { expectedScopes: ['read:all_data']} },
            // { path: 'components', loadChildren: './bs-component/bs-component.module#BsComponentModule' },
            // { path: 'blank-page', loadChildren: './blank-page/blank-page.module#BlankPageModule' }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}

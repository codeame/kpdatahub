import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClickMeetingComponent } from './clickmeeting.component';

describe('ClickMeetingComponent', () => {
    let component: ClickMeetingComponent;
    let fixture: ComponentFixture<ClickMeetingComponent>;

    beforeEach(
        async(() => {
            TestBed.configureTestingModule({
                declarations: [ClickMeetingComponent]
            }).compileComponents();
        })
    );

    beforeEach(() => {
        fixture = TestBed.createComponent(ClickMeetingComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});

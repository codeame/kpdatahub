/* Copyright (C) 2018 Miguel Chavez Gamboa
 miguel@codea.me

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { routerTransition } from '../../router.animations';

import { IWebinar } from '../../../user';
import { ApiService } from '../../shared';
import { AuthService } from '../../shared/services/auth/auth.service';

import { DatePipe } from '@angular/common';
import { DatatableComponent } from '@swimlane/ngx-datatable';

import { DomSanitizer } from '@angular/platform-browser';

import { expandCollapse } from '../../shared/expand-collapse.animation';

import * as XLSX from 'xlsx';

import * as Fuse from 'fuse.js';

@Component({
    selector: 'clickmeeting-page',
    templateUrl: './clickmeeting.component.html',
    styleUrls: ['./clickmeeting.component.scss'],
    providers: [ApiService],
    animations: [routerTransition(), expandCollapse],
    encapsulation: ViewEncapsulation.None
})
export class ClickMeetingComponent implements OnInit {
    public webinars: IWebinar;
    public empty_rooms:any = [];
    public unfilteredWebinars:any=[];
    public profile:any;
    public rows: any = [];

    public filtered:any = [];
    btnFiltersText = 'Hide Filters';
    showFilters:boolean = true;
    
    public columns: any = [{prop:'Date'}, {name:'Name'}, {name:'Attende Count'}];
    timeout: any;

    public filterOptions: any = {
        filterByAccountOrTitle:true,
        filterByTheAttendeeNumber: true,
        filterByAttendeeCount:1,
        filterByAttendees:'More',
        filterByAttendeeText:'',
        filterByTheDate: false,
        filterByText: '',
        filterByDate: 'After'
    }

    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(private apiService: ApiService, private sanitizer: DomSanitizer, public auth:AuthService) {
    }

    onChangeDate(event) {
        // When date changes on the datepicker
        this.updateFilter({only_date:true, date:event});
    }

    toggleFilters() {
        this.showFilters = !this.showFilters;
        this.btnFiltersText = this.showFilters ? 'Hide Filters' : 'Show Filters';
    }

    getTableHeight() {
        if (this.showFilters) {
            // console.log('INNER HEIGHT:', window.innerHeight);
            return this.sanitizer.bypassSecurityTrustStyle('height: calc(80vh - 260px); overflow-x: scroll; max-width: 84vw;');
        } else {
            return this.sanitizer.bypassSecurityTrustStyle('height: calc(80vh - 40px); overflow-x: scroll; max-width: 84vw;  ');
        }
    }

    updateFilter(event) {
        var filter:boolean = false;
        var fTemp:any = [];
        const fOptions = this.filterOptions;
        var val:any;

        if (typeof event.only_date === "undefined"  ) {
            // Vienen mas datos
            val = event.target.value.toLowerCase();
            //console.log('TARGET:', target, ' VAL:', val, ' Filtered size:', this.filtered.length);
        } else {
            val = '';
            // console.log('event:', event);
        }
        

        console.log('STARTING == fTemp:', fTemp.length);        
        fTemp = this.webinars.webinar_attendees; // reset filter

        if ( fOptions.filterByTheAttendeeNumber ) {
            filter = true;
            var num = this.filterOptions.filterByAttendeeCount;
            if ( fOptions.filterByAttendees == 'More' ) {
                // console.log('Filter by MORE  1 fTemp BEFORE Filter 1A:', fTemp.length);
                fTemp = fTemp.filter(function(d) {
                    return d.count > num;   
                });
            } else if (fOptions.filterByAttendees == 'Less' ) {
                // console.log('Filter LESS but not set.   2 fTemp BEFORE Filter 1B:', fTemp.length);
                fTemp = fTemp.filter(function(d) {
                    return d.count <= num;
                });
            }
            console.log('Filtering Stage 1 Result:', fTemp.length);
        }

        if ( fOptions.filterByText.length ) {
            filter = true;
            // console.log('Filtering by NAME:', fOptions.filterByText, '2 fTemp BEFORE 2B:', fTemp.length);
            fTemp = fTemp.filter(function(d) {
                return d.room_name.toLowerCase().indexOf( fOptions.filterByText.toLowerCase() ) !== -1 || !val;
            });
            // console.log('Filtering Stage 2 Result:', fTemp.length);
        }

        if ( fOptions.filterByAttendeeText.length ) {
            filter = true;
            console.log('Filtering by Attendees:', fOptions.filterByAttendeeText, '2 fTemp BEFORE 2B:', fTemp.length);

            fTemp = fTemp.filter( elemento => {
                var options = {
                    shouldSort: true,
                    threshold: 0.0,
                    location: 0,
                    distance: 100,
                    maxPatternLength: 32,
                    minMatchCharLength: 1,
                    keys: ["email", "nickname"]
                  };
                  var fuse = new Fuse(elemento.attendees, options);
                  var result = fuse.search( fOptions.filterByAttendeeText.toLowerCase() );
                  if ( result.length ) {console.log('Resultado: ', result);}
                  return result.length || !fOptions.filterByAttendeeText;
            } );
            
            // console.log('Filtering Stage 2 Result:', fTemp.length);
        }

        if ( fOptions.filterByTheDate ) {
            filter = true;
            if ( fOptions.filterByDate == 'After' ) {
                // console.log('Filtering by DATE:', fOptions.filterByDate, ' : ', fOptions.filterByDateStr, ' 3 fTemp BEFORE Filter 3A :', fTemp.length);
                fTemp = fTemp.filter(function(d) {
                    if ( d.date !== undefined ) {
                        var the_date:Date = new Date( d.date );
                        var filter_date = new Date( fOptions.filterByDateStr.year, fOptions.filterByDateStr.month-1, fOptions.filterByDateStr.day );
                        // console.log('date from model:', the_date, ' Date to Filter:', filter_date);
                        // FIXME: Cambiar operador con radiobuttons ( >=, <=,  ==, !=)
                        if ( the_date > filter_date ) {
                            return true;
                        } else {
                            return false;
                        }
                        
                    } else return false;
                });
            } else if ( fOptions.filterByDate == 'Before' ) {
                // console.log('Filtering by DATE:', fOptions.filterByDate, ' : ', fOptions.filterByDateStr, ' 3 fTemp BEFORE Filter 3A :', fTemp.length);
                fTemp = fTemp.filter(function(d) {
                    if ( d.date !== undefined ) {
                        var the_date:Date = new Date( d.date );
                        var filter_date = new Date( fOptions.filterByDateStr.year, fOptions.filterByDateStr.month-1, fOptions.filterByDateStr.day );
                        // console.log('date from model:', the_date, ' Date to Filter:', filter_date);
                        // FIXME: Cambiar operador con radiobuttons ( >=, <=,  ==, !=)
                        if ( the_date < filter_date ) {
                            return true;
                        } else {
                            return false;
                        }
                        
                    } else return false;
                });
            }
        } 


        if (filter) {
            // table already filtered and updated
            // console.log('* fTemp (SI):', fTemp.length);
            this.rows = fTemp; //this.filtered;

        } else {
            // unfilter
            // console.log('* fTemp (NO):', fTemp.length);
            this.rows = this.unfilteredWebinars;
        }
        // console.log('FilterOptions:', this.filterOptions);
        // console.log('Count:', this.rows.length );
    }

    getClickMeetingWebinars(): void {
        this.apiService.getClickMeetingWebinars()
            .subscribe(
                resultArray => { 
                    this.webinars = resultArray;
                    this.empty_rooms = resultArray.empty_rooms;
                    this.unfilteredWebinars = [].concat( resultArray.webinar_attendees );

                    // push our inital complete list
                    this.rows = this.webinars.webinar_attendees;
                    console.log('WEBINARS: ',resultArray.webinar_attendees);
                    console.log('Firing filter...');
                    this.updateFilter({ target:{value:this.filterOptions.filterByText} });
                },
                error => console.log('Error :: ' + error)
            );
    }

    exportXLS(target) {
        console.log('exporting to XLS:',target);
        var ws; 
        var title;
        switch(target) {
            case 'webinars':
                var tmp_rows = [];
                this.rows.forEach( webinar => {
                    var tmp_asist = [];
                    var asistentes = webinar['attendees'];
                    asistentes.forEach( asistente => {
                        tmp_asist.push( asistente['email'] );
                    });
                    tmp_rows.push( {'count': webinar['count'], 'date':webinar['date'], 'room_name':webinar['room_name'], 'attendees':tmp_asist.join(', ') } );
                });
                ws = XLSX.utils.json_to_sheet( tmp_rows );
                title = 'Webinars';
                break;
            case 'empty_rooms':
                // console.log('Empty webinars:', this.empty_rooms );
                ws = XLSX.utils.json_to_sheet( this.empty_rooms );
                title = 'Empty Webinars';
                break;
        }
        var wb = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, title);
        XLSX.writeFile(wb, "clickmeeting_webinars.xlsx");
    };

    ngOnInit() {
        if (this.auth.userProfile) {
            this.profile = this.auth.userProfile;
        } else {
            this.auth.getProfile((err, profile) => {
                this.profile = profile;
            });
        }
        this.getClickMeetingWebinars();
    }
}

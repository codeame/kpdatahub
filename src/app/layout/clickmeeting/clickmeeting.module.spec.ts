import { ClickMeetingModule } from './clickmeeting.module';

describe('ClickMeetingModule', () => {
    let clickmeetingPageModule: ClickMeetingModule;

    beforeEach(() => {
        clickmeetingPageModule = new ClickMeetingModule();
    });

    it('should create an instance', () => {
        expect(clickmeetingPageModule).toBeTruthy();
    });
});

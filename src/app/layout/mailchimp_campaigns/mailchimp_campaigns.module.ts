/* Copyright (C) 2018 Miguel Chavez Gamboa
 miguel@codea.me

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MailChimpRoutingModule } from './mailchimp_campaigns-routing.module';
import { MailChimpComponent } from './mailchimp_campaigns.component';

// import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { InfiniteScrollModule } from 'ngx-infinite-scroll';

@NgModule({
    imports: [CommonModule, MailChimpRoutingModule,  FormsModule, NgbModule.forRoot(), InfiniteScrollModule ],
    declarations: [MailChimpComponent]
})
export class MailChimpModule {}

/* Copyright (C) 2018 Miguel Chavez Gamboa
 miguel@codea.me

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { routerTransition } from '../../router.animations';

import { ICampaigns } from '../../../user';
import {ApiService} from '../../shared';
import { AuthService } from '../../shared/services/auth/auth.service';

import { DatePipe } from '@angular/common';
import {DatatableComponent} from '@swimlane/ngx-datatable';

import { DomSanitizer } from '@angular/platform-browser';

import { expandCollapse } from '../../shared/expand-collapse.animation';

import * as XLSX from 'xlsx';

@Component({
    selector: 'mailchimp-page',
    templateUrl: './mailchimp_campaigns.component.html',
    styleUrls: ['./mailchimp_campaigns.component.scss'],
    providers: [ApiService],
    animations: [routerTransition(), expandCollapse],
    encapsulation: ViewEncapsulation.None
})
export class MailChimpComponent implements OnInit {
    public campaigns: ICampaigns;
    public new_rows:any = [];
    public unfilteredCampaigns: any = [];
    public unfilteredXls: any = [];
    public xlsRows: any = [];
    public profile:any;
    public rows: any = [];
    public altura: any = this.sanitizer.bypassSecurityTrustUrl('height: 80vh;  max-height:80vh;');
    
    public page: number = 0;
    public isFiltered: boolean = false;
    public filtered:any = [];
    btnFiltersText = 'Hide Filters';
    showFilters:boolean = true;

    public columns: any = [{prop:'Account'}, {name:'Status'}, {name:'Title'}, {name:'Date created'}, {name:'Date Sent'}, {name:'From name'}, {name:'Emails sent'}, {name:'Opens'}, {name:'Clicks'}];
    timeout: any;

    public filterOptions: any = {
        filterByAccountOrTitle:false,
        filterByTheStatus: false,
        filterByTheDate: false,
        filterByAccount: 'Account',
        filterByText: '',
        filterByDate: 'SentAfter',
        filterByStatus: 'More'
    }
    
    @ViewChild(DatatableComponent) table: DatatableComponent;
    
    constructor(private apiService: ApiService, private sanitizer: DomSanitizer, public auth:AuthService) {
    }

    toggleFilters() {
        this.showFilters = !this.showFilters;
        this.btnFiltersText = this.showFilters ? 'Hide Filters' : 'Show Filters';
    }

    getTableHeight() {
        if (this.showFilters) {
            // console.log('INNER HEIGHT:', window.innerHeight);
            return this.sanitizer.bypassSecurityTrustStyle('height: calc(80vh - 340px); overflow-x: scroll; max-width: 84vw;');
        } else {
            return this.sanitizer.bypassSecurityTrustStyle('height: calc(80vh - 80px); overflow-x: scroll; max-width: 84vw;  ');
        }
    }

    onScroll() {
        this.page = this.page +1;
        console.log('scrolled. PAGE:', this.page);
        // get data from api
        this.getCampaigns();
    }
    
    onChangeDate(event) {
        // When date changes on the datepicker
        this.updateFilter({only_date:true, date:event});
    }


    exportXLS() {
        console.log('exporting to XLS...');
        // var fecha_str = moment(value.fecha_str).format('MMMM YYYY')
        
        var wb = XLSX.utils.book_new();
        var ws = XLSX.utils.json_to_sheet( this.xlsRows );
        XLSX.utils.book_append_sheet(wb, ws, 'ClickMeeting Attendees');
        XLSX.writeFile(wb, "clickmeeting_attendees.xlsx");
    };

    updateFilter(event) {
        var filter:boolean = false;
        var fTemp:any = [];
        var fTempXls:any = [];
        const fOptions = this.filterOptions;
        var val:any;

        if (typeof event.only_date === "undefined"  ) {
            // Vienen mas datos
            val = event.target.value.toLowerCase();
            //console.log('TARGET:', target, ' VAL:', val, ' Filtered size:', this.filtered.length);
        } else {
            val = '';
            // console.log('event:', event);
        }
        

        console.log('STARTING == fTemp:',fTemp.length);        
        fTemp = this.unfilteredCampaigns; // reset filter
        fTempXls = this.unfilteredXls;

        if ( fOptions.filterByTheStatus ) {
            filter = true;
            if ( fOptions.filterByStatus == 'Sent' ) {
                // console.log('Filter by SENT  1 fTemp BEFORE Filter 1A:', fTemp.length);
                fTemp = fTemp.filter(function(d) {
                    return d._id.status.toLowerCase().indexOf( 'sent' ) !== -1;   
                });
                fTempXls = fTempXls.filter(function(d) {
                    return d.status.toLowerCase().indexOf( 'sent' ) !== -1;   
                });
            } else if (fOptions.filterByStatus == 'NotSent' ) {
                // console.log('Filter SAVED but not set.   2 fTemp BEFORE Filter 1B:', fTemp.length);
                fTemp = fTemp.filter(function(d) {
                    return d._id.status.toLowerCase().indexOf( 'save' ) !== -1;   
                });
                fTempXls = fTempXls.filter(function(d) {
                    return d.status.toLowerCase().indexOf( 'save' ) !== -1;   
                });
            } else if (fOptions.filterByStatus == 'Less' ) {
                // console.log('Filter EMAILS SENT < 3000.   2 fTemp BEFORE Filter 1B:', fTemp.length);
                fTemp = fTemp.filter(function(d) {
                    return d.emails_sent < 3000;
                });
                fTempXls = fTempXls.filter(function(d) {
                    return d.emails_sent < 3000;
                });
            } else if (fOptions.filterByStatus == 'More' ) {
                // console.log('Filter EMAILS SENT > 3000.   2 fTemp BEFORE Filter 1B:', fTemp.length);
                fTemp = fTemp.filter(function(d) {
                    return d.emails_sent >= 3000;
                });
                fTempXls = fTempXls.filter(function(d) {
                    return d.emails_sent >= 3000;
                });
            }
            console.log('Filtering Stage 1 Result:', fTemp.length, ' Xls:',fTempXls.length);
        }

        if ( fOptions.filterByAccountOrTitle ) {
            filter = true;
            if ( fOptions.filterByAccount == 'Account' ) {
                // console.log('Filtering by ACCOUNT NAME:', fOptions.filterByText, ' x:', fOptions.filterByAccount, ' 2 fTemp BEFORE Filter 2 A:', fTemp.length);
                fTemp = fTemp.filter(function(d) {
                    if ( d.account_name !== null && d.account_name !== undefined ) {
                        var text_to_search = fOptions.filterByText.toLowerCase();
                        var lista = d.account_name;
                        var matches = lista.filter( texto => texto.toLowerCase().indexOf( text_to_search ) !== -1 );
                        return matches.length;
                    } else {
                        return false;
                    }
                    //return d.account_name.toLowerCase().indexOf( fOptions.filterByText.toLowerCase() ) !== -1 || !val;   
                });
                fTempXls = fTempXls.filter(function(d) {
                    if ( d.account_name !== null && d.account_name !== undefined ) {
                        var text_to_search = fOptions.filterByText.toLowerCase();
                        var lista = d.account_list;
                        var matches = lista.filter( texto => texto.toLowerCase().indexOf( text_to_search ) !== -1 );
                        return matches.length;
                    } else {
                        return false;
                    }
                    //return d.account_name.toLowerCase().indexOf( fOptions.filterByText.toLowerCase() ) !== -1 || !val;   
                });
            } else if (fOptions.filterByAccount == 'Title' ) {
                // console.log('Filtering by TITLE:', fOptions.filterByText, '2 fTemp BEFORE 2B:', fTemp.length);
                fTemp = fTemp.filter(function(d) {
                    return d._id.title.toLowerCase().indexOf( fOptions.filterByText.toLowerCase() ) !== -1 || !val;
                });
                fTempXls = fTempXls.filter(function(d) {
                    return d.title.toLowerCase().indexOf( fOptions.filterByText.toLowerCase() ) !== -1 || !val;
                });
            }
            console.log('Filtering Stage 2 Result:', fTemp.length, ' Xls:',fTempXls.length);
        }

        if ( fOptions.filterByTheDate ) {
            filter = true;
            if ( fOptions.filterByDate == 'SentAfter' ) {
                console.log('Filtering by SEND DATE:', fOptions.filterByDate, ' : ', fOptions.filterByDateStr, ' 3 fTemp BEFORE Filter 3A :', fTemp.length);
                fTemp = fTemp.filter(function(d) {
                    if ( d._id.send_time !== undefined ) {
                        // var the_date:Date = new Date( d.send_time.$date );
                        // var filter_date = new Date( fOptions.filterByDateStr.year, fOptions.filterByDateStr.month-1, fOptions.filterByDateStr.day );
                        var the_date:Date = new Date( d._id.send_time );
                        var filter_date = new Date( fOptions.filterByDateStr.year, fOptions.filterByDateStr.month-1, fOptions.filterByDateStr.day );
                        // console.log('date from model:', the_date, ' Date to Filter:', filter_date);
                        // FIXME: Cambiar operador con radiobuttons ( >=, <=,  ==, !=)
                        if ( the_date > filter_date ) {
                            return true;
                        } else {
                            return false;
                        }
                        
                    } else return false;
                });
                fTempXls = fTempXls.filter(function(d) {
                    if ( d.send_time !== undefined ) {
                        var the_date:Date = new Date( d.send_time );
                        var filter_date = new Date( fOptions.filterByDateStr.year, fOptions.filterByDateStr.month-1, fOptions.filterByDateStr.day );
                        // console.log('date from model:', the_date, ' Date to Filter:', filter_date);
                        // FIXME: Cambiar operador con radiobuttons ( >=, <=,  ==, !=)
                        if ( the_date > filter_date ) {
                            return true;
                        } else {
                            return false;
                        }
                        
                    } else return false;
                });
            } else if ( fOptions.filterByDate == 'CreatedAfter' ) {
                // console.log('Filtering by CREATED DATE:', fOptions.filterByDate, ' : ', fOptions.filterByDateStr, ' 3 fTemp BEFORE Filter 3B :', fTemp.length);
                fTemp = fTemp.filter(function(d) {
                    if ( d._id.send_time !== undefined ) {
                        // var the_date:Date = new Date( d.create_time.$date );
                        // var filter_date = new Date( fOptions.filterByDateStr.year, fOptions.filterByDateStr.month-1, fOptions.filterByDateStr.day );
                        var the_date:Date = new Date( d._id.send_time );
                        var filter_date = new Date( fOptions.filterByDateStr.year, fOptions.filterByDateStr.month-1, fOptions.filterByDateStr.day );
                        // console.log('date from model:', the_date, ' Date to Filter:', filter_date);
                        // FIXME: Cambiar operador con radiobuttons ( >=, <=,  ==, !=)
                        if ( the_date > filter_date ) {
                            return true;
                        } else {
                            return false;
                        }
                        
                    } else return false;
                });
                fTempXls = fTempXls.filter(function(d) {
                    if ( d.send_time !== undefined ) {
                        var the_date:Date = new Date( d.create_time );
                        var filter_date = new Date( fOptions.filterByDateStr.year, fOptions.filterByDateStr.month-1, fOptions.filterByDateStr.day );
                        // console.log('date from model:', the_date, ' Date to Filter:', filter_date);
                        // FIXME: Cambiar operador con radiobuttons ( >=, <=,  ==, !=)
                        if ( the_date > filter_date ) {
                            return true;
                        } else {
                            return false;
                        }
                        
                    } else return false;
                });
            }

            if ( fOptions.filterByDate == 'SentBefore' ) {
                console.log('Filtering by SEND DATE:', fOptions.filterByDate, ' : ', fOptions.filterByDateStr, ' 3 fTemp BEFORE Filter 3A :', fTemp.length);
                fTemp = fTemp.filter(function(d) {
                    if ( d._id.send_time !== undefined ) {
                        // var the_date:Date = new Date( d.send_time.$date );
                        // var filter_date = new Date( fOptions.filterByDateStr.year, fOptions.filterByDateStr.month-1, fOptions.filterByDateStr.day );
                        var the_date:Date = new Date( d._id.send_time );
                        var filter_date = new Date( fOptions.filterByDateStr.year, fOptions.filterByDateStr.month-1, fOptions.filterByDateStr.day );
                        // console.log('date from model:', the_date, ' Date to Filter:', filter_date);
                        // FIXME: Cambiar operador con radiobuttons ( >=, <=,  ==, !=)
                        if ( the_date < filter_date ) {
                            return true;
                        } else {
                            return false;
                        }
                        
                    } else return false;
                });
                fTempXls = fTempXls.filter(function(d) {
                    if ( d.send_time !== undefined ) {
                        var the_date:Date = new Date( d.send_time );
                        var filter_date = new Date( fOptions.filterByDateStr.year, fOptions.filterByDateStr.month-1, fOptions.filterByDateStr.day );
                        // console.log('date from model:', the_date, ' Date to Filter:', filter_date);
                        // FIXME: Cambiar operador con radiobuttons ( >=, <=,  ==, !=)
                        if ( the_date < filter_date ) {
                            return true;
                        } else {
                            return false;
                        }
                        
                    } else return false;
                });
            } else if ( fOptions.filterByDate == 'CreatedBefore' ) {
                // console.log('Filtering by CREATED DATE:', fOptions.filterByDate, ' : ', fOptions.filterByDateStr, ' 3 fTemp BEFORE Filter 3B :', fTemp.length);
                fTemp = fTemp.filter(function(d) {
                    if ( d._id.send_time !== undefined ) {
                        // var the_date:Date = new Date( d.create_time.$date );
                        // var filter_date = new Date( fOptions.filterByDateStr.year, fOptions.filterByDateStr.month-1, fOptions.filterByDateStr.day );
                        var the_date:Date = new Date( d._id.send_time );
                        var filter_date = new Date( fOptions.filterByDateStr.year, fOptions.filterByDateStr.month-1, fOptions.filterByDateStr.day );
                        // console.log('date from model:', the_date, ' Date to Filter:', filter_date);
                        // FIXME: Cambiar operador con radiobuttons ( >=, <=,  ==, !=)
                        if ( the_date < filter_date ) {
                            return true;
                        } else {
                            return false;
                        }
                        
                    } else return false;
                });
                fTempXls = fTempXls.filter(function(d) {
                    if ( d.send_time !== undefined ) {
                        var the_date:Date = new Date( d.create_time );
                        var filter_date = new Date( fOptions.filterByDateStr.year, fOptions.filterByDateStr.month-1, fOptions.filterByDateStr.day );
                        // console.log('date from model:', the_date, ' Date to Filter:', filter_date);
                        // FIXME: Cambiar operador con radiobuttons ( >=, <=,  ==, !=)
                        if ( the_date < filter_date ) {
                            return true;
                        } else {
                            return false;
                        }
                        
                    } else return false;
                });
            }
            this.isFiltered = filter;
            console.log('Filtering Stage 3 Results:', fTemp.length, ' Xls:',fTempXls.length);
        }


        if (filter) {
            // table already filtered and updated
            // console.log('* fTemp (SI):', fTemp.length);
            this.rows = fTemp; //this.filtered;
            this.xlsRows = fTempXls;
        } else {
            // unfilter
            // console.log('* fTemp (NO):', fTemp.length);
            this.rows = this.unfilteredCampaigns; //this.campaigns.campaigns;
            this.xlsRows = this.unfilteredXls;
        }
        // console.log('FilterOptions:', this.filterOptions);
        // console.log('Count:', this.rows.length );
    }


    getCampaigns(): void {
        this.apiService.getMailChimpCampaigns({'page':this.page})
            .subscribe(
                resultArray => { 
                    this.campaigns = resultArray;

                    // push our inital complete list
                    this.rows = this.campaigns.campaigns;
                    console.log('CAMPAIGNS: ',resultArray.campaigns);

                    this.new_rows = resultArray.campaigns;
                    // this.total_attendees = resultArray.totals;
                    // if filters are applied, remove and then re apply
                    if ( this.isFiltered ) {
                        console.log('Numero de campaigns antes de unfilter: ', this.rows.length);
                        this.rows = [].concat(this.unfilteredCampaigns);
                        console.log('Filtro aplicado, restaurando campaigns sin filtro: ', this.rows.length, ' SIN FILTRO ORIGINAL:', this.unfilteredCampaigns.length);
                    }
                    // Push new rows to the whole list
                    this.rows = this.rows.concat(this.new_rows);
                    // Push new rows to the unfilteredCampaigns...
                    this.unfilteredCampaigns = this.unfilteredCampaigns.concat( this.new_rows );
                    // Now create a copy for XLS export whit less data
                    this.new_rows.forEach(element => {
                        this.xlsRows.push( {
                            'account_name': element['account_name'].join(', '), 
                            'account_list': element['account_name'],
                            'click_rate':element['click_rate'], 
                            'clicks':element['clicks'], 
                            'open_rate':element['open_rate'], 
                            'unique_opens':element['unique_opens'], 
                            'emails_sent':element['emails_sent'], 
                            'send_time': element['_id']['send_time'], 
                            'status': element['_id']['status'],
                            'title': element['_id']['title'],
                        });
                    });
                    this.unfilteredXls = [].concat( this.xlsRows );
                    console.log('XLS ROWS:', this.xlsRows.length);
                    console.log('XLS Unfiltered:', this.unfilteredXls.length );
                    // Now, filter if any filter is active
                    console.log('Firing filter...');
                    this.updateFilter({ target:{value:this.filterOptions.filterByText} });
                },
                error => console.log('Error :: ' + error)
            );
    }
    

    ngOnInit() {
        if (this.auth.userProfile) {
            this.profile = this.auth.userProfile;
          } else {
            this.auth.getProfile((err, profile) => {
              this.profile = profile;
            });
          }
        this.getCampaigns();
    }
}

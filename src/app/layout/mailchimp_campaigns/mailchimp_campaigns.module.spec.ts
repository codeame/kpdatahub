import { MailChimpModule } from './mailchimp_campaigns.module';

describe('MailChimpCampaignModule', () => {
    let mailchimpPageModule: MailChimpModule;

    beforeEach(() => {
        mailchimpPageModule = new MailChimpModule();
    });

    it('should create an instance', () => {
        expect(mailchimpPageModule).toBeTruthy();
    });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClickMeetingUsersComponent } from './clickmeeting_users.component';

describe('ClickMeetingUsersComponent', () => {
    let component: ClickMeetingUsersComponent;
    let fixture: ComponentFixture<ClickMeetingUsersComponent>;

    beforeEach(
        async(() => {
            TestBed.configureTestingModule({
                declarations: [ClickMeetingUsersComponent]
            }).compileComponents();
        })
    );

    beforeEach(() => {
        fixture = TestBed.createComponent(ClickMeetingUsersComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});

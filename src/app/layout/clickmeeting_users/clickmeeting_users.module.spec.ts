import { ClickMeetingUsersModule } from './clickmeeting_users.module';

describe('ClickMeetingUsersModule', () => {
    let clickmeetingPageModule: ClickMeetingUsersModule;

    beforeEach(() => {
        clickmeetingPageModule = new ClickMeetingUsersModule();
    });

    it('should create an instance', () => {
        expect(clickmeetingPageModule).toBeTruthy();
    });
});

/* Copyright (C) 2018 Miguel Chavez Gamboa
 miguel@codea.me

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { routerTransition } from '../../router.animations';

import { IClickMeetingUsers } from '../../../user';
import {ApiService} from '../../shared';
import { AuthService } from '../../shared/services/auth/auth.service';

import { DatePipe } from '@angular/common';

// import {DatatableComponent} from '@swimlane/ngx-datatable';

import { DomSanitizer } from '@angular/platform-browser';
import { expandCollapse } from '../../shared/expand-collapse.animation';


import * as XLSX from 'xlsx';
import { forEach } from '@angular-devkit/schematics';

@Component({
    selector: 'clickmeeting-users-page',
    templateUrl: './clickmeeting_users.component.html',
    styleUrls: ['./clickmeeting_users.component.scss'],
    providers: [ApiService],
    animations: [
        routerTransition(),
        expandCollapse
      ],
    encapsulation: ViewEncapsulation.None
})
export class ClickMeetingUsersComponent implements OnInit {
    public users: IClickMeetingUsers;
    public unfilteredUsers:any = [];
    public unfilteredXls: any = [];
    public xlsRows: any = [];
    public profile:any;
    public rows: any = [];
    public total_attendees: number = 0;
    public new_rows: any = [];
    public page: number = 0;
    public isFiltered: boolean = false;
    public altura: any = this.sanitizer.bypassSecurityTrustStyle('height: 80vh;  max-height:80vh;');
    
    public filtered:any = [];
    btnFiltersText = 'Hide Filters';
    showFilters:boolean = true;

    public columns: any = [{prop:'email'}, {name:'nickname'}, {name:'role'}, {name:'webinar_count'}];
    timeout: any;

    public filterOptions: any = {
        filterByAccountOrTitle:false,
        filterByTheWebinar: false,
        filterByTheDate: false,
        filterByAccount: 'Email',
        filterByText: '',
        filterByWebinarText: '',
        filterByDate: 'CreatedAfter',
        filterByStatus: 'Listener',
        filterByUserTypeRb:false,
        filterByUserType: 'both'
    }
    

    // @ViewChild(DatatableComponent) table: DatatableComponent;
    
    constructor(private apiService: ApiService, private sanitizer: DomSanitizer, public auth:AuthService) {
        this.page = 0;
    }

    toggleFilters() {
        this.showFilters = !this.showFilters;
        this.btnFiltersText = this.showFilters ? 'Hide Filters' : 'Show Filters';
    }

    getTableHeight() {
        if (this.showFilters) {
            // console.log('INNER HEIGHT:', window.innerHeight);
            return this.sanitizer.bypassSecurityTrustStyle('height: calc(80vh - 280px); overflow-x: scroll; max-width: 84vw;');
        } else {
            return this.sanitizer.bypassSecurityTrustStyle('height: calc(80vh - 80px); overflow-x: scroll; max-width: 84vw;  ');
        }
    }

    
    onScroll() {
        this.page = this.page +1;
        console.log('scrolled. PAGE:', this.page);
        // get data from api
        this.getClickMeetingUsers();
    }

    // exportCSV() {
    //     console.log('exporting CSV...');
    //     // CSV
    //     var options = { 
    //         fieldSeparator: ',',
    //         quoteStrings: '"',
    //         decimalseparator: '.',
    //         showLabels: true, 
    //         showTitle: true,
    //         title: 'ClickMeeting Attendees',
    //         useBom: true,
    //         noDownload: false,
    //         headers: ["Email", "Nickname", "Country"]
    //       };
    //     new Angular5Csv(this.rows, 'ClickMeeting_Attendees', options);
    // };

    exportXLS() {
        console.log('exporting to XLS...');
        // var fecha_str = moment(value.fecha_str).format('MMMM YYYY')
    
        // var tmp_rows = [].concat(this.xlsRows);
        // console.log('Antes de parchar:',this.xlsRows);
        // tmp_rows.forEach( attendee => {
        //     attendee['webinars'] = attendee['webinars'].join(', ');
        //     delete attendee['webinar_list'];
        // });
        // console.log('Despues de parchar:',this.xlsRows);

        var wb = XLSX.utils.book_new();
        var ws = XLSX.utils.json_to_sheet( this.xlsRows );
        XLSX.utils.book_append_sheet(wb, ws, 'ClickMeeting Attendees');
        XLSX.writeFile(wb, "clickmeeting_attendees.xlsx");
    };


    onChangeDate(event) {
        // When date changes on the datepicker
        this.updateFilter({only_date:true, date:event});
    }

    updateFilter(event) {
        var filter:boolean = false;
        var fTemp:any = [];
        var fTempXls:any = [];
        const fOptions = this.filterOptions;
        var val:any;

        if (typeof event.only_date === "undefined"  ) {
            // Vienen mas datos
            val = event.target.value.toLowerCase();
            //console.log('TARGET:', target, ' VAL:', val, ' Filtered size:', this.filtered.length);
        } else {
            val = '';
            // console.log('event:', event);
        }
        

        // console.log('STARTING == fTemp:',fTemp.length, ' xlsTemp:',fTempXls.length);
        // reset filter
        fTemp = this.unfilteredUsers; 
        fTempXls = this.unfilteredXls;


        console.log('STARTING == fTemp:',fTemp.length, ' xlsTemp:',fTempXls.length);
        if ( fOptions.filterByTheWebinar ) {
            filter = true;
            fTemp = fTemp.filter(function(d) {
                if ( d.webinars !== null && d.webinars !== undefined ) {
                    var text_to_search = fOptions.filterByWebinarText.toLowerCase();
                    var lista = d.webinars;
                    var matches = lista.filter( texto => texto.toLowerCase().indexOf( text_to_search ) !== -1 );
                    return matches.length;
                } else {
                    return false;
                }
            });
            fTempXls = fTempXls.filter(function(d) {
                if ( d.webinars !== null && d.webinars !== undefined ) {
                    var text_to_search = fOptions.filterByWebinarText.toLowerCase();
                    var lista = d.webinars;
                    var matches = lista.filter( texto => texto.toLowerCase().indexOf( text_to_search ) !== -1 );
                    return matches.length;
                } else {
                    return false;
                }
            });
            console.log('Filtering Stage 1 Result:', fTemp.length, ' Xls:',fTempXls.length);
        }

        if ( fOptions.filterByAccountOrTitle ) {
            filter = true;
            if ( fOptions.filterByAccount == 'Email' ) {
                console.log('Filtering by Email:', fOptions.filterByText, ' x:', fOptions.filterByAccount, ' 2 fTemp BEFORE Filter 2 A:', fTemp.length);
                fTemp = fTemp.filter(function(d) {
                    if ( d.email !== null ) {
                        return d.email.toLowerCase().indexOf( fOptions.filterByText.toLowerCase() ) !== -1 || !val;
                    } else {
                        return false;
                    }
                });
                fTempXls = fTempXls.filter(function(d) {
                    if ( d.email !== null ) {
                        return d.email.toLowerCase().indexOf( fOptions.filterByText.toLowerCase() ) !== -1 || !val;
                    } else {
                        return false;
                    }
                });
            } else if (fOptions.filterByAccount == 'Nickname' ) {
                console.log('Filtering by NICKNAME:', fOptions.filterByText, '2 fTemp BEFORE 2B:', fTemp.length);
                fTemp = fTemp.filter(function(d) {
                    if ( d.email !== null ) {
                        return d.nickname.toLowerCase().indexOf( fOptions.filterByText.toLowerCase() ) !== -1 || !val;
                    } else {
                        return false;
                    }
                });
                fTempXls = fTempXls.filter(function(d) {
                    if ( d.email !== null ) {
                        return d.nickname.toLowerCase().indexOf( fOptions.filterByText.toLowerCase() ) !== -1 || !val;
                    } else {
                        return false;
                    }
                });
            }
            console.log('Filtering Stage 2 Result:', fTemp.length, ' Xls:',fTempXls.length);
        }

        if ( fOptions.filterByUserTypeRb ) {
            filter = true;
            if ( fOptions.filterByUserType == 'both' ) {
                // console.log('Filtering by Email:', fOptions.filterByText, ' x:', fOptions.filterByAccount, ' 2 fTemp BEFORE Filter 2 A:', fTemp.length);
                fTemp = this.unfilteredUsers;
                fTempXls = this.unfilteredXls;
            } else if (fOptions.filterByUserType == 'clickmeeting' ) {
                // console.log('Filtering by clickmeeting only:', fOptions.filterByText, '2 fTemp BEFORE 2B:', fTemp.length);
                fTemp = fTemp.filter(function(d) {
                    if ( d.kp_user !== null && d.kp_user !== undefined ) {
                        return !d.kp_user;
                    } else {
                        return false;
                    }
                });
                fTempXls = fTempXls.filter(function(d) {
                    // console.log('d:',d);
                    if ( d['KP User'] !== null && d['KP User'] !== undefined ) {
                        // console.log('kp_user:',d['KP User']);
                        return d['KP User'] == 'NO';
                    } else {
                        return false;
                    }
                });
            } else if (fOptions.filterByUserType == 'gpsa' ) {
                // console.log('Filtering by gpsa only:', fOptions.filterByText, '2 fTemp BEFORE 2B:', fTemp.length);
                fTemp = fTemp.filter(function(d) {
                    if ( d.kp_user !== null && d.kp_user !== undefined ) {
                        return d.kp_user;
                    } else {
                        return false;
                    }
                });
                fTempXls = fTempXls.filter(function(d) {
                    // console.log('d:',d);
                    if ( d['KP User'] !== null && d['KP User'] !== undefined ) {
                        // console.log('kp_user:',d['KP User']);
                        return d['KP User'] == 'YES';
                    } else {
                        return false;
                    }
                });
            }
            console.log('Filtering Stage 2 Result:', fTemp.length, ' Xls:',fTempXls.length);
        }

        
        if (filter) {
            this.rows = fTemp; //this.filtered;
            this.xlsRows = fTempXls;
        } else {
            // unfilter
            this.rows = this.unfilteredUsers;
            this.xlsRows = this.unfilteredXls;
        }
        this.isFiltered = filter;
        console.log('Num de usuarios FILTRADOS:', this.rows.length, ' XLS Filtrados:', this.xlsRows.length);
    }


    getClickMeetingUsers(): void {
        this.apiService.getClickMeetingUsers({'page':this.page})
            .subscribe(
                resultArray => { 
                    this.new_rows = resultArray.users;
                    this.total_attendees = resultArray.totals;
                    console.log('attendees: ', resultArray.users);
                    // if filters are applied, remove and then re apply
                    if ( this.isFiltered ) {
                        console.log('Numero de usuarios antes de unfilter: ', this.rows.length);
                        this.rows = [].concat(this.unfilteredUsers);
                        console.log('Filtro aplicado, restaurando usuarios sin filtro: ', this.rows.length, ' SIN FILTRO ORIGINAL:', this.unfilteredUsers.length);
                    }
                    // Push new rows to the whole list
                    this.rows = this.rows.concat(this.new_rows);
                    // Push new rows to the unfilteredUsers...
                    this.unfilteredUsers = this.unfilteredUsers.concat( this.new_rows );
                    // Now create a copy for XLS export whit less data
                    this.new_rows.forEach(element => {
                        // FIXME: xlsRows son TODOS, sin filtro.
                        var kp_user = '';
                        if (element['kp_user']) { kp_user = 'YES' } else { kp_user = 'NO' }
                        this.xlsRows.push( {'email': element['email'], 'Name':element['nickname'], 'nickname':element['nickname'], 'Registered':element['registered'], 'Country':element['country'], 'KP User': kp_user, 'Logins': element['login_count'],  'role':element['role'], 'Webinars Count':element['webinar_count'], 'Minutes': element['minutes'], 'Organization': element['organization'], 'Organization Type': element['org_type'], 'Position in Org.': element['position'], 'webinars':element['webinars'], 'webinar_list':element['webinars'].join(', ') } )
                    });
                    this.unfilteredXls = [].concat( this.xlsRows );
                    console.log('XLS ROWS:', this.xlsRows.length);
                    console.log('XLS Unfiltered:', this.unfilteredXls.length );
                    // Now, filter if any filter is active
                    console.log('Firing filter...');
                    this.updateFilter({ target:{value:this.filterOptions.filterByText} });
                },
                error => console.log('Error :: ' + error)
            );
    }

    ngOnInit() {
        if (this.auth.userProfile) {
            this.profile = this.auth.userProfile;
          } else {
            this.auth.getProfile((err, profile) => {
              this.profile = profile;
            });
          }
        this.getClickMeetingUsers();
    }
}

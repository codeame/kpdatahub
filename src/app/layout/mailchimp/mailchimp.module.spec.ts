import { MailChimpModule } from './mailchimp.module';

describe('MailChimpModule', () => {
    let mailchimpPageModule: MailChimpModule;

    beforeEach(() => {
        mailchimpPageModule = new MailChimpModule();
    });

    it('should create an instance', () => {
        expect(mailchimpPageModule).toBeTruthy();
    });
});

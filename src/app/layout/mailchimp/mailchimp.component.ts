/* Copyright (C) 2018 Miguel Chavez Gamboa
 miguel@codea.me

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import { Component, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../../router.animations';

import { ICampaigns } from '../../../user';
import {ApiService} from '../../shared';

import { DatePipe } from '@angular/common';
import {DatatableComponent} from '@swimlane/ngx-datatable';

@Component({
    selector: 'mailchimp-page',
    templateUrl: './mailchimp.component.html',
    styleUrls: ['./mailchimp.component.scss'],
    providers: [ApiService],
    animations: [routerTransition()]
})
export class MailChimpComponent implements OnInit {
    public campaigns: ICampaigns;
    public rows: any = [];
    temp:any = [];
    public columns: any = [{prop:'Account'}, {name:'Status'}, {name:'Title'}, {name:'Date created'}, {name:'Date Sent'}, {name:'From name'}, {name:'Emails sent'}, {name:'Opens'}, {name:'Clicks'}];
    timeout: any;
    @ViewChild(DatatableComponent) table: DatatableComponent;
    constructor(private apiService: ApiService) {
    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();
    
        // filter our data
        const temp = this.temp.filter(function(d) {
          return d.account_name.toLowerCase().indexOf(val) !== -1 || !val;
        });
    
        // update the rows
        this.rows = temp;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    getCampaigns(): void {
        this.apiService.getMailChimpCampaigns({})
            .subscribe(
                resultArray => { 
                    this.campaigns = resultArray;
                    this.temp = this.campaigns.campaigns;

                    // push our inital complete list
                    this.rows = this.campaigns.campaigns;
                    // this.rows = [{
                    //     "account_name": "marinetransitional",
                    //     "cid": "0d207f505f",
                    //     "create_time": {
                    //       "$date": 1480095303000
                    //     },
                    //     "emails_sent": 75,
                    //     "from_name": "Happening today: The Interdisciplinary Group of Independent Experts (GIEI) experience as an alternative mechanism to fight impunity in Mexico",
                    //     "report_summary": {
                    //       "click_rate": 0.04,
                    //       "clicks": 14,
                    //       "ecommerce": {
                    //         "total_orders": 0,
                    //         "total_revenue": 0,
                    //         "total_spent": 0
                    //       },
                    //       "open_rate": 0.24,
                    //       "opens": 134,
                    //       "subscriber_clicks": 3,
                    //       "unique_opens": 18
                    //     },
                    //     "send_time": {
                    //       "$date": 1480095529000
                    //     },
                    //     "status": "sent",
                    //     "title": "Webinar Ayotzinapa - Happening Today",
                    //     "web_id": 686173
                    //   }, {
                    //     "account_name": "ASDFmarinetransitional",
                    //     "cid": "0d207f505f",
                    //     "create_time": {
                    //       "$date": 1480095303000
                    //     },
                    //     "emails_sent": 75,
                    //     "from_name": "Happening today: The Interdisciplinary Group of Independent Experts (GIEI) experience as an alternative mechanism to fight impunity in Mexico",
                    //     "report_summary": {
                    //       "click_rate": 0.04,
                    //       "clicks": 14,
                    //       "ecommerce": {
                    //         "total_orders": 0,
                    //         "total_revenue": 0,
                    //         "total_spent": 0
                    //       },
                    //       "open_rate": 0.24,
                    //       "opens": 134,
                    //       "subscriber_clicks": 3,
                    //       "unique_opens": 18
                    //     },
                    //     "send_time": {
                    //       "$date": 1480095529000
                    //     },
                    //     "status": "sent",
                    //     "title": "Webinar Ayotzinapa - Happening Today",
                    //     "web_id": 686173
                    //   }];
                    
                    console.log('CAMPAIGNS: ',resultArray);
                },
                error => console.log('Error :: ' + error)
            );
    }

    ngOnInit() {
        this.getCampaigns();
    }
}

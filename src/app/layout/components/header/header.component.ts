
/* Copyright (C) 2018 Miguel Chavez Gamboa
 miguel@codea.me

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { AuthService } from './../../../shared/services/auth/auth.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    pushRightClass: string = 'push-right';
    profile:any;

    constructor(private translate: TranslateService, public router: Router, public auth:AuthService) {

        this.translate.addLangs(['en']);
        this.translate.setDefaultLang('en');
        // const browserLang = this.translate.getBrowserLang();
        this.translate.use('en');
        console.log('LANG:',this.translate.currentLang );
        // this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de/) ? browserLang : 'en');

        this.router.events.subscribe(val => {
            if (
                val instanceof NavigationEnd &&
                window.innerWidth <= 992 &&
                this.isToggled()
            ) {
                this.toggleSidebar();
            }
        });
    }

    ngOnInit() {
        if (this.auth.userProfile) {
            this.profile = this.auth.userProfile;
            console.log(' ngInit :: PROFILE:',this.profile);
          } else {
            this.auth.getProfile((err, profile) => {
              this.profile = profile;
              console.log(' NO Profile, getting it -- ngInit :: PROFILE:',this.profile);
            });
          }
    }

    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    onLoggedout() {
        localStorage.removeItem('isLoggedin');
    }

    onLoggedin() { 
        console.log('OnLoggedIn::getting user info');
        this.auth.getProfile((err, profile) => {
            this.profile = profile;
            console.log(' NO Profile, getting it -- onLoggedIn :: PROFILE:',this.profile);
        });
    }

    get expiresAt() {
        return JSON.parse(window.localStorage.getItem('expires_at'));
      }

    changeLang(language: string) {
        this.translate.use(language);
    }
}

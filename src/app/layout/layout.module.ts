import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { HeaderComponent } from './components/header/header.component';

import { MomentModule } from 'angular2-moment';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
// import { NgbDropdownModule} from '@ng-bootstrap/ng-bootstrap';
// import { NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
    imports: [
        CommonModule,
        LayoutRoutingModule,
        TranslateModule,
        NgbModule,
        // NgbTabsetModule,
        // NgbModule.forRoot(),
        // NgbDropdownModule.forRoot(),
        // NgbTabsetModule, //.forRoot(),
        MomentModule
    ],
    declarations: [
        LayoutComponent, SidebarComponent, HeaderComponent //, TabsComponent
    ]
})
export class LayoutModule {}

/* Copyright (C) 2018 Miguel Chavez Gamboa
 miguel@codea.me

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';

import { IUsers, IAttendees, IStatistics } from '../../../user';
import {ApiService} from '../../shared';
import { UtilsService } from '../../utils.service';
import { AuthService } from '../../shared/services/auth/auth.service';

import { expandCollapse } from '../../shared/expand-collapse.animation';

import * as XLSX from 'xlsx';


@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    providers: [ApiService],
    animations: [routerTransition(), expandCollapse]
})
export class DashboardComponent implements OnInit {
    public statistics: IStatistics;
    public users: IUsers[];
    public attendees: IAttendees[];
    public options: {};
    public profile: any;
    loading: boolean;

    //charts
    // lineChart
    public lineChartData: Array<any> = [
        { data: [1,3,4,5,6,7], label: 'Knowledge Platform' },
        { data: [28, 48, 40, 19, 86, 27, 90], label: 'ClickMeeting' }
    ];
    public lineChartLabels: Array<any> = ['one', 'two'];
    public lineChartOptions: any = {
        responsive: true
    };
    public lineChartColors: Array<any> = [
        {
            // grey
            backgroundColor: 'rgba(148,159,177,0.2)',
            borderColor: 'rgba(148,159,177,1)',
            pointBackgroundColor: 'rgba(148,159,177,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        },
        {
            // dark grey
            backgroundColor: 'rgba(77,83,96,0.2)',
            borderColor: 'rgba(77,83,96,1)',
            pointBackgroundColor: 'rgba(77,83,96,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(77,83,96,1)'
        },
        {
            // grey
            backgroundColor: 'rgba(148,159,177,0.2)',
            borderColor: 'rgba(148,159,177,1)',
            pointBackgroundColor: 'rgba(148,159,177,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        }
    ];
    public lineChartLegend: boolean = true;
    public lineChartType: string = 'line';

    // events
    public chartClicked(e: any): void {
        // console.log(e);
    }

    public chartHovered(e: any): void {
        // console.log(e);
    }
    //charts

    constructor(private apiService: ApiService, public auth:AuthService, public utils: UtilsService) {
        // this.statistics = [];
        // this.users = [];
        // this.attendees = [];
        this.options = { keywords:'' };
        
    }

    getStatistics(): void {
        this.loading = true;
        this.apiService.getStatistics()
            .subscribe(
                resultArray => { 
                    this.loading = false;
                    this.statistics = resultArray; 
                    console.log('Statistics ARRAY:',resultArray);
                    // this.lineChartData = resultArray['chart_data']; //chart_data
                    // this.lineChartLabels = resultArray['users_etiquetas'];
                    // console.log('ALTERNO:', this.lineChartData, 'Labels:', this.lineChartLabels);
                },
                
                error => {
                    console.log('Error :: ' + error);
                    this.loading = false;
                }
            );
    }

    getUsers(options: {}): void {
        this.apiService.getUsers(options)
            .subscribe(
                resultArray => { this.users = resultArray; console.log('Users ARRAY:',resultArray);},
                error => console.log('Error :: ' + error)
            );
    }


    exportXLS(target) {
        console.log('exporting to XLS: ',target);
        
        var wb = XLSX.utils.book_new();
        var ws;
        var fn;
        var titulo:string;
        switch( target ) {
            case 'super_users':
                ws = XLSX.utils.json_to_sheet( this.statistics.super_users );
                titulo = 'Super Users';
                fn = 'kp_super_users.xlsx';
                console.log('Exporting super users');
                break;
            case 'lurker_users':
                ws = XLSX.utils.json_to_sheet( this.statistics.lurker_users );
                titulo = 'Lurker Users';
                fn = 'kp_lurker_users.xlsx';
                console.log('Exporting lurker users');
                break;
            case 'arrepentidos':
                ws = XLSX.utils.json_to_sheet( this.statistics.arrepentidos );
                titulo = 'Non returning users';
                fn = 'kp_nonreturning_users.xlsx';
                console.log('Exporting non returning users');
                break;
            case 'users_only_one_webinar':
                ws = XLSX.utils.json_to_sheet( this.statistics.users_only_one_webinar );
                titulo = 'One Webinar Users';
                fn = 'kp_onewebinar_users.xlsx';
                console.log('Exporting one webinar users');
                break;
            case 'webinar_superuser':
                ws = XLSX.utils.json_to_sheet( this.statistics.webinar_superuser );
                titulo = 'Clickmeeting only Super Users';
                fn = 'clickmeeting_super_users.xlsx';
                console.log('Exporting webinar super users');
                break;
            case 'onetime_webinar_users':
                ws = XLSX.utils.json_to_sheet( this.statistics.onetime_webinar_users );
                console.log('Exporting one time webinar users');
                fn = 'clickmeeting_onewebinar_users.xlsx';
                titulo = 'One time webinar users';
                break;
            case 'chart_data':
                ws = XLSX.utils.json_to_sheet( this.statistics.alterno['data'] );
                console.log('Exporting chart data');
                fn = 'KP User registration timeline.xlsx';
                titulo = 'Users registration over time';
                break;
        }
        
        XLSX.utils.book_append_sheet(wb, ws, titulo);
        XLSX.writeFile(wb, fn);    
    };



    ngOnInit() {
        if (this.auth.userProfile) {
            this.profile = this.auth.userProfile;
          } else {
            this.auth.getProfile((err, profile) => {
              this.profile = profile;
            });
          }
        this.getStatistics();
    }

    public closeAlert(alert: any) {
        const index: number = 0; //this.alerts.indexOf(alert);
        // this.alerts.splice(index, 1);
    }
}

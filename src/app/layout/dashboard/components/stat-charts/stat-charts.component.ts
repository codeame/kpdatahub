import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../../router.animations';


import { IUsers, IAttendees, IStatistics } from '../../../../../user';
import {ApiService} from '../../../../shared';


@Component({
    selector: 'app-stat-chart',
    templateUrl: './stat-charts.component.html',
    styleUrls: ['./stat-charts.component.scss'],
    providers: [ApiService],
    animations: [routerTransition()]
})
export class StatChartsComponent implements OnInit {
    constructor() { }
    ngOnInit() { }
}

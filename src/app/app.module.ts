import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuard, ScopeGuardService } from './shared';

import {ApiService} from './shared';
import { AuthService } from './shared/services/auth/auth.service';
import { CallbackComponent } from './callback/callback.component';


import { NgProgressModule } from '@ngx-progressbar/core';
import { NgProgressHttpModule } from '@ngx-progressbar/http';


import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NgbDropdownModule} from '@ng-bootstrap/ng-bootstrap';
import { NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';

// Auth0 JWT
import { JwtModule } from '@auth0/angular-jwt';

import { UtilsService } from './utils.service';

export function tokenGetter() {
  const token:any = localStorage.getItem('access_token');
  return token;
}

// AoT requires an exported function for factories
export function createTranslateLoader(http: HttpClient) {
    // for development
    // return new TranslateHttpLoader(http, '/start-angular/SB-Admin-BS4-Angular-5/master/dist/assets/i18n/', '.json');
    return new TranslateHttpLoader(http, 'https://gpsa.codea.me/static/assets/i18n/', '.json');
}

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        NgbModule.forRoot(),
        NgbDropdownModule.forRoot(),
        JwtModule.forRoot({
            config: {
                tokenGetter: tokenGetter,
                whitelistedDomains: ['gpsa.reporta.me', 'localhost:5000'],
                //blacklistedRoutes: ['localhost:3001/auth/'],
                //throwNoTokenError: true,
                skipWhenExpired: true
            }
        }),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        }),
        AppRoutingModule,
        NgProgressModule.forRoot(),
        NgProgressHttpModule
    ],
    declarations: [AppComponent, CallbackComponent],
    providers: [AuthGuard, ScopeGuardService, ApiService, AuthService, UtilsService],
    bootstrap: [AppComponent]
})
export class AppModule {}

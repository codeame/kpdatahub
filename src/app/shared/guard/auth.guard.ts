import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';

import { AuthService } from '../../shared/services/auth/auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private router: Router, public auth:AuthService) {}


    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        if (this.auth.isAuthenticated() ) {
          return true;
        } else {
          // Send guarded route to redirect to after logging in
          this.auth.login(state.url);
          return false;
        }
      }

    // canActivate() {
    //     // Using Auth0
    //     if ( this.auth.isAuthenticated() ) {
    //         return true;
    //     }
        
    //     this.auth.login();
    //     return false;
    // }
}

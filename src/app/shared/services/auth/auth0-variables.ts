import { environment } from '../../../../environments/environment';

interface AuthConfig {
  clientID: string;
  domain: string;
  callbackURL: string;
}

var the_url:string = '';

if (environment.production) {
  the_url =  'https://gpsa.codea.me/callback';
} else {
  the_url = 'http://localhost:4200/callback'; 
}

export const AUTH_CONFIG: AuthConfig = {
  clientID: 'ofWqgT1BlEZWIu5M8ZrtIhUgk9OYGGYe',
  domain: 'gpsaanalytics.auth0.com',
  callbackURL: the_url
};
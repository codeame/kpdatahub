import { Injectable } from '@angular/core';
import { AUTH_CONFIG } from './auth0-variables';
import { Router } from '@angular/router';

import { Observable } from 'rxjs';
import 'rxjs/add/operator/filter';

import * as auth0 from 'auth0-js';

@Injectable()
export class AuthService {

  // define the refreshSubscription property
  refreshSubscription: any;
  userProfile: any;
  userInfo:any;

  requestedScopes: string = 'openid profile email read:statistics read:all_data';

  auth0 = new auth0.WebAuth({
    clientID: AUTH_CONFIG.clientID,
    domain: AUTH_CONFIG.domain,
    responseType: 'token id_token',
    audience: 'http://gpsa.reporta.me',
    redirectUri: AUTH_CONFIG.callbackURL,
    scope: this.requestedScopes
  }); // audience: `https://${AUTH_CONFIG.domain}/userinfo`,

  constructor(public router: Router) {}

  public login(redirect?: string): void {
    // Set redirect after login
    const _redirect = redirect ? redirect : this.router.url;
    localStorage.setItem('authRedirect', _redirect);

    this.auth0.authorize();
  }

  private _clearRedirect() {
    // Remove redirect from localStorage
    localStorage.removeItem('authRedirect');
  }

  public handleAuthentication(): void {
    this.auth0.parseHash((err, authResult) => {
      if (authResult && authResult.accessToken && authResult.idToken) {
        console.log('AUTHRESULT:',authResult);
        this.setSession(authResult);
        this.router.navigate(['/dashboard']);
      } else if (err) {
        this._clearRedirect();
        this.router.navigate(['/']);
        console.log(err);
        //alert(`Error: ${err.error}. Check the console for further details.`);
      }
    });
  }

  private setSession(authResult): void {
    // Set the time that the access token will expire at
    const expiresAt = JSON.stringify((authResult.expiresIn * 1000) + new Date().getTime());
    const scopes = authResult.scope || this.requestedScopes || '';

    localStorage.setItem('access_token', authResult.accessToken);
    localStorage.setItem('id_token', authResult.idToken);
    localStorage.setItem('expires_at', expiresAt);
    localStorage.setItem('scopes', JSON.stringify(scopes));
    // MCH: Agregando info del usuario
    const self = this;
    this.auth0.client.userInfo(authResult.accessToken, function(err, user) {
      // Now you have the user's information
      console.log('USER INFO:',user);
      localStorage.setItem('user_info', user);
      self.userProfile = user;
    });
    this.scheduleRenewal();
  }

  public logout(): void {
    // Remove tokens and expiry time from localStorage
    localStorage.removeItem('access_token');
    localStorage.removeItem('id_token');
    localStorage.removeItem('expires_at');
    localStorage.removeItem('scopes');
    localStorage.removeItem('user_info');
    
    this._clearRedirect();

    this.unscheduleRenewal();
    // Go back to the home route
    this.router.navigate(['/']);
  }

  public isAuthenticated(): boolean {
    // Check whether the current time is past the
    // access token's expiry time
    const expiresAt = JSON.parse(localStorage.getItem('expires_at'));
    
    //MCH: Add aditional checks with data from server like permited users?
    //MCH: Now auth includes scopes for permissions...

    return new Date().getTime() < expiresAt;
  }

  public renewToken() {
    this.auth0.checkSession({}, (err, result) => {
      if (err) {
        console.log(err);
      } else {
        this.setSession(result);
      }
    });
  }

  public scheduleRenewal() {
    if (!this.isAuthenticated()) { return; }
    this.unscheduleRenewal();

    const expiresAt = JSON.parse(window.localStorage.getItem('expires_at'));

    const source = Observable.of(expiresAt).flatMap(
      expiresAt => {

        const now = Date.now();

        // Use the delay in a timer to
        // run the refresh at the proper time
        return Observable.timer(Math.max(1, expiresAt - now));
      });

    // Once the delay time from above is
    // reached, get a new JWT and schedule
    // additional refreshes
    this.refreshSubscription = source.subscribe(() => {
      this.renewToken();
      this.scheduleRenewal();
    });

  }

  public unscheduleRenewal() {
    if (this.refreshSubscription) {
      this.refreshSubscription.unsubscribe();
    }
  }

  public getProfile(cb): void {
    const accessToken = localStorage.getItem('access_token');
    if (!accessToken) {
      throw new Error('Access token must exist to fetch profile');
    }

    const self = this;
    this.auth0.client.userInfo(accessToken, (err, profile) => {
      if (profile) {
        self.userProfile = profile;
        if ( localStorage.getItem('authRedirect') != null) { //MCH
          console.log('GOING TO REDIRECT:', localStorage.getItem('authRedirect'));
          this.router.navigate([localStorage.getItem('authRedirect') || '/']);
          // this._redirect(); // for supporting query parameters (tabs)
          this._clearRedirect();
        }
      }
      cb(err, profile);
    });
  }


  // private _redirect() {
  //   // Redirect with or without 'tab' query parameter
  //   // Note: does not support additional params besides 'tab'
  //   const fullRedirect = decodeURI(localStorage.getItem('authRedirect'));
  //   const redirectArr = fullRedirect.split('?tab=');
  //   const navArr = [redirectArr[0] || '/'];
  //   const tabObj = redirectArr[1] ? { queryParams: { tab: redirectArr[1] }} : null;

  //   if (!tabObj) {
  //     this.router.navigate(navArr);
  //   } else {
  //     this.router.navigate(navArr, tabObj);
  //   }
  // }

  public userHasScopes(scopes: Array<string>): boolean {
    const grantedScopes = JSON.parse(localStorage.getItem('scopes')).split(' ');
    return scopes.every(scope => grantedScopes.includes(scope));
  }

}

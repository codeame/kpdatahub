import {Injectable} from '@angular/core';
import { IUsers, IAttendees, IStatistics, ICampaigns, IWebinar, IClickMeetingUsers, IKpUsers } from '../../../../user';
import { HttpModule, Response, URLSearchParams }    from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';

import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';

import { environment } from '../../../../environments/environment';

@Injectable()
export class ApiService {

  public  _postsURL:any;
 
  constructor(private http: HttpClient) {
    if (  environment.production ) {
      this._postsURL =  'https://gpsa.reporta.me/';
    } else {
      this._postsURL = 'http://localhost:5000/'; 
      // this._postsURL =  'https://gpsa.reporta.me/';
    }
  }

  isLoaded(loading: boolean): boolean {
    return loading === false;
  }

  getUsers(options: {}): Observable<IUsers[]> {
    // let _options_users: URLSearchParams = new URLSearchParams();
    // _options_users.set('params', '{ "$or" : [ {"about":{"$regex":"mentoring"}},  {"about": {"$regex":"evaluation"}}, {"about": {"$regex":"learning"}},  {"about": {"$regex":"MEL"}}, {"about": {"$regex":"M&amp;L"}}, {"about": {"$regex":"M &amp; L"}}, {"about": {"$regex":"M & L"}}]}');

    // _options_users.set('params', '{ "$or" : [ {"about":{"$regex":"youth"}},  {"about": {"$regex":"participation"}}, {"about": {"$regex":"learning"}}]}');

    console.log('OPTIONS:',options);

      return this.http.post<IUsers[]>(this._postsURL+'users', options)
    // return this.http
    //       .post(this._postsURL+'users', options )
    //         .map((response: Response) => {
    //           console.log('RESPONSE FROM API users:', response);
    //           return <IUsers[]>response.json();
    //         })
    //       .catch(this.handleError);

    // return this.http
    //       .get(this._postsURL+'users', {search:_options_users} )
    //       .map((response: Response) => {
    //           console.log('RESPONSE FROM API users:', response);
    //           return <IUsers[]>response.json();
    //       })
    //       .catch(this.handleError);
  }

  getAttendees(): Observable<IAttendees[]> {
    let _options_att: URLSearchParams = new URLSearchParams();
    _options_att.set('params', '{ "$or" : [ {"description":{"$regex":"acff"}},  {"about": {"$regex":"Financial Manager"}}]}');

    return this.http.get<IAttendees[]>(this._postsURL+'attendees')

    // return this.http
    //       .get(this._postsURL+'attendees')
    //       .map((response: Response) => {
    //           console.log('RESPONSE FROM API attendees:', response);
    //           return <IAttendees[]>response.json();
    //       })
    //       .catch(this.handleError);
  }

  getStatistics(): Observable<IStatistics> {
    
    return this.http
          .get<IStatistics>(this._postsURL+'statistics')
          // .map((response: Response) => {
          //     console.log('RESPONSE FROM API statistics:', response);
          //     return <IStatistics[]>response.json();
          // })
          // .catch(this.handleError);
  }

  getMailChimpCampaigns(page: {}): Observable<ICampaigns> {
    return this.http
          .post<ICampaigns>(this._postsURL+'mailchimp/campaigns', page)
  }

  getClickMeetingWebinars(): Observable<IWebinar> {
    return this.http
          .get<IWebinar>(this._postsURL+'clickmeeting/webinars')
  }

  getClickMeetingUsers(page: {}): Observable<IClickMeetingUsers> {
    // console.log('Service::getClickMeetingUsers PAGE:', page);
    return this.http
          .post<IClickMeetingUsers>(this._postsURL+'clickmeeting/attendees', page)
  }

  getKpUsers(page: {}): Observable<IKpUsers> {
    // console.log('Service::getKpUsers PAGE:', page);
    return this.http
          .post<IKpUsers>(this._postsURL+'kp/users', page)
  }

  private handleError(error: Response) {
      console.log('ERROR: -->',error);
      return Observable.throw(error.statusText);
  }
}




